"""
High-level tests for the  package.

"""

import lhc_longitudinal_tomography_ml_ucap


def test_version():
    assert lhc_longitudinal_tomography_ml_ucap.__version__ is not None
