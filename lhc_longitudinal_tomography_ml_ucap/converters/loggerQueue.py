import functools

from ucap.common import (
    AcquiredParameterValue,
    ValueType,
    FailSafeParameterValue,
    ValueHeader,
    Event)
from queue import Queue
from typing import List
import logging


class LoggerQueue(logging.Handler):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.message_queue = Queue()

    def emit(self, record) -> None:
        self.message_queue.put(self.format(record).rstrip("\n"))

    def getQueuedMessages(self) -> List[str]:
        q = []
        while not self.message_queue.empty():
            q.append(self.message_queue.get())
        return q

    def toFspv(self, parameter_name: str, header: ValueHeader):
        apv_logger = AcquiredParameterValue(parameter_name, header)
        apv_logger.update_value(
            "messages", self.getQueuedMessages(), ValueType.STRING_ARRAY
        )
        return FailSafeParameterValue(apv_logger)

    def publish(self, convert_func):
        @functools.wraps(convert_func)
        def wrapped_convert(event: Event):
            published_properties = convert_func(event)
            import inspect
            module = inspect.getmodule(convert_func)
            header = event.trigger_value.header if event.trigger_value else ValueHeader()
            return [self.toFspv(f"{module.device_name}/Logger_{module.transformation_name}", header)] + \
                   published_properties
        return wrapped_convert

