import numpy as np
from ucap.common.context import device_name, transformation_name, configuration
from typing import List
from ucap.common import (
    AcquiredParameterValue,
    Event,
    FailSafeParameterValue,
    ValueHeader,
    ValueType,
)
import logging

from mlp_lhc_tomography import LHCTomoscope
from mlp_lhc_tomography.utils import minmax_normalize_param, unnormalize_params
from mlp_lhc_tomography.utils import correctForTriggerOffset
from mlp_client import Client, Profile, AUTO

from lhc_longitudinal_tomography_ml_ucap.converters.loggerQueue import LoggerQueue


logger = logging.getLogger(__name__)
logger_queue = LoggerQueue()
logger.addHandler(logger_queue)
logger.info(f"Logger name: {__name__}")


encDec = None


def configuration_post_processing():
    pass


@logger_queue.publish
def convert(event: Event) -> List[FailSafeParameterValue]:
    global encDec

    if encDec is None:
        encDec = Client(Profile.PRO).create_model(
            model_class=LHCTomoscope,
            params_name="mlp_lhc_tomography.LHCTomoscope",
            params_version=AUTO,
            verbose=True,
        )

    IMG_OUTPUT_SIZE = encDec.model.inputShape[0]
    zeropad = 14
    skipturns = 3
    bunch_samples = IMG_OUTPUT_SIZE - 2 * zeropad

    try:
        header = event.trigger_value.header if event.trigger_value else ValueHeader()
        properties_to_notify = []

        p: AcquiredParameterValue = event.trigger_value.value
        filled_buckets = p.get_value("filled_buckets")
        bunch_profiles = p.get_value("bunch_profiles")
        time_ax = p.get_value("time_ax")

        imgs = np.empty((len(filled_buckets), IMG_OUTPUT_SIZE, IMG_OUTPUT_SIZE, 1))
        turn_min, turn_max = configuration["normalization_factors"]["turn_min_max"]
        norm_turn = minmax_normalize_param(0, turn_min, turn_max)
        turns = np.array([norm_turn] * len(filled_buckets))

        for b_id in range(len(filled_buckets)):
            b_img = np.reshape(
                bunch_profiles[b_id],
                (int(bunch_profiles.shape[1] / len(time_ax)), len(time_ax)),
            ).transpose()

            b_img = (
                b_img
                / np.sum(b_img, axis=0)
                * configuration["scope_to_model_params"]["Ib"]
            )
            if configuration["scope_to_model_params"]["corrTriggerOffset"]:
                b_img = correctForTriggerOffset(
                    time_ax,
                    b_img,
                    filter_n=configuration["scope_to_model_params"]["filter_n"],
                    iterations=configuration["scope_to_model_params"]["iterations"],
                )
            norm_img = b_img / configuration["normalization_factors"]["T_normFactor"]
            norm_img = norm_img[:, ::skipturns][:, :bunch_samples]
            norm_img = np.pad(
                norm_img,
                (
                    (
                        zeropad - configuration["scope_correction"]["centroid_offset"],
                        zeropad + configuration["scope_correction"]["centroid_offset"],
                    ),
                    (zeropad, zeropad),
                ),
                "constant",
                constant_values=(0, 0),
            )
            imgs[b_id, :, :, 0] = norm_img

        outputs = encDec.predict({"inputs": imgs, "turns": turns})

        unnorm_latents = unnormalize_params(
            outputs["latents"][:, 0],
            outputs["latents"][:, 1],
            outputs["latents"][:, 2],
            outputs["latents"][:, 3],
            outputs["latents"][:, 4],
            outputs["latents"][:, 5],
            outputs["latents"][:, 6],
            normalization="minmax",
        )

        logger.info(f"Converted {len(filled_buckets)} profiles")

        out_apv = AcquiredParameterValue(f"{device_name}/DecodedParameters", header)
        for i, latent_name in enumerate(encDec.model.encoder.var_names):
            out_apv.update_value(latent_name, unnorm_latents[i].numpy())
        eridx = list(encDec.model.encoder.var_names).index("enEr")
        out_apv.update_value("normEnEr", unnorm_latents[eridx].numpy())
        properties_to_notify.append(out_apv)
    except:
        logger.exception("Error while converting the profiles")

    return [FailSafeParameterValue(apv) for apv in properties_to_notify]
