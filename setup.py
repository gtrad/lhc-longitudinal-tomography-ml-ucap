"""
setup.py for lhc-longitudinal-tomography-ml-ucap.

For reference see
https://packaging.python.org/guides/distributing-packages-using-setuptools/

"""
import ast
import os
from pathlib import Path
from setuptools import setup, find_packages

HERE = Path(__file__).parent.absolute()
ressource_path = Path(__file__).parent.absolute()

with (HERE / 'README.md').open('rt') as fh:
    LONG_DESCRIPTION = fh.read().strip()


def get_version_from_init():
    init_file = os.path.join(
        os.path.join(
            os.path.dirname(__file__), "lhc_longitudinal_tomography_ml_ucap", "__init__.py"
        )
    )
    with open(init_file, "r") as fd:
        for line in fd:
            if line.startswith("__version__"):
                return ast.literal_eval(line.split("=", 1)[1].strip())


VERSION = get_version_from_init()

REQUIREMENTS: dict = {
    'core': [
        'tensorflow==2.14',
        'mlp_lhc_tomography',
    ],
    'test': [
        'pytest',
    ],
    'dev': [
        'ucap',
        'tensorflow==2.14',
        'mlp_lhc_tomography',
        'matplotlib',
    ],
    'doc': [
        # 'sphinx',
        # 'acc-py-sphinx',
    ],
}

setup(
    name='lhc-longitudinal-tomography-ml-ucap',
    version=VERSION,

    author='gtrad',
    author_email='georges.trad@cern.ch',
    description='SHORT DESCRIPTION OF PROJECT',
    long_description=LONG_DESCRIPTION,
    long_description_content_type='text/markdown',
    url='',

    packages=find_packages(),
    python_requires='>=3.6, <4',
    classifiers=[
        "Programming Language :: Python :: 3",
        "Operating System :: OS Independent",
    ],

    install_requires=REQUIREMENTS['core'],
    extras_require={
        **REQUIREMENTS,
        # The 'dev' extra is the union of 'test' and 'doc', with an option
        # to have explicit development dependencies listed.
        'dev': [req
                for extra in ['dev', 'test', 'doc']
                for req in REQUIREMENTS.get(extra, [])],
        # The 'all' extra is the union of all requirements.
        'all': [req for reqs in REQUIREMENTS.values() for req in reqs],
    }
)
